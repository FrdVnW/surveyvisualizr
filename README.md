
# Table of Contents

1.  [Introduction](#org9283243)
2.  [Installation](#org2518c37)
3.  [Usage](#org15a21b0)
    1.  [Loading libraries](#org88f32c9)
    2.  [Importing data](#org195c5ea)
    3.  [Launch the application](#org39c6ec6)
4.  [Screencast : tutorial for the usage of the application](#orgcf3034b)



<a id="org9283243"></a>

# Introduction

The goal of surveyvisualizr is to visualize data collected from
survey. This package is working in R and is completed by a shiny 
app. 


<a id="org2518c37"></a>

# Installation

surveyvisualizr is on development and hosted on Gitlab. Using the package
'devtools', you can install it with the command : 

    devtools::install_git('https://gitlab.com/FrdVnW/surveyvisualizr', upgrade_dependencies = FALSE)

Till now, this package is only working for data collected using
Limesurvey, and more precisely in the framework of the DiverIMPACTS project.


<a id="org15a21b0"></a>

# Usage


<a id="org88f32c9"></a>

## Loading libraries

When you have installed the package in R and all its dependencies, you
have to load them.

    rm(list=ls())
    here::here()
    library(surveyvisualizr)
    
    library(tidyr)
    library(dplyr)
    library(shiny)
    library(plotly)
    library(DT)


<a id="org195c5ea"></a>

## Importing data

Now, you will need to copy the data files ('.csv') in the data-raw folder and to
import them.

    ## Only for DiverIMPACTS project
    ## -- N.B. --
    ## Copy the two data files :
    ##  - survey_438634_R_data_file.csv
    ##  - survey_438634_R_data_file-names.csv
    ## 
    ## in the 'data-raw' folder of your working directory
    
    source('R/load-di.R')
    load.di()


<a id="org39c6ec6"></a>

## Launch the application

You can launch the application with the following command.

    ## Launch the application
    ## ======================
    surveyvisualizr()


<a id="orgcf3034b"></a>

# Screencast : tutorial for the usage of the application

<iframe width="560" height="315" src="https://www.youtube.com/embed/DkSa78KmH2Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Link to the video : <https://youtu.be/DkSa78KmH2Y>

