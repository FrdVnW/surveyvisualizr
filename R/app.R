## Shiny App
## =========

## app.survey.viz <- function(port=8008,host='my.ip') {
##     if (host == 'my.ip') {
##         my.ip <- system("ip a | grep 'inet ' | grep -v '127.0.0.1' | grep -oE '((1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])\\.){3}(1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])' | head -1",intern=TRUE)
##         my.host <- my.ip
##         if (length(my.host) == 0) {
##             my.host <- '127.0.0.1'}
##     } else {
##         my.host <- '127.0.0.1'
##     }
##     source("inst/shiny-surveyviz/ui.R")
##     source("inst/shiny-surveyviz/server.R")
##     options(shiny.port = port)
##     options(shiny.host = my.host)
##     app <- shinyApp(ui, server)
##     runApp(app)
## }

#' This launches survizr, the surveyvisualizr Shiny app
#'
#' @examples
#' \dontrun{
#'  survizr()
#' }
#' @export
survizr <- function() {
  package_location <<- system.file(package = "surveyvisualizr")
  shiny::runApp(paste0(package_location, "/shiny/surveyvisualizr"))
}
