## create the server
server <- function(input, output) {
    ## Reactive input fields (left panel)
    ## ----------------------------------

    ## === Subquestions (for mapping) ===
    ## ---> Canceled : replace by 'criteria'
    ## output$v.question.mapping <- renderUI({
    ##     df.questions.mapping <- df.questions %>%
    ##         dplyr::filter(questions == input$questions) %>%
    ##         dplyr::filter(class == "factor")
    
    ##     selectInput("questions.mapping",
    ##                 label = "Sub-question (item)",
    ##                 choices = df.questions.mapping$item
    ##                 )
    ## })

    ## === Criteria ===
    output$v.questions.criteria.1 <- renderUI({

        ## df.questions.factors <- df.questions %>% dplyr::filter(class=="factor") %>% dplyr::select(questions) %>% dplyr::pull()
        df.questions.factors <- df.questions$class == "factor"
        
        selectInput("questions.factors.criteria.1",
                    label = "Big question or computed variable",
                    ## choices = c("No comparison or computed variables"="FALSE",l.questions),
                    choices = c("No comparison or computed variables"="FALSE",l.questions[df.questions.factors]),
                    selected = "no comparison"
                    )
    })

    output$v.criteria.1 <- renderUI({
        if (req(input$questions.factors.criteria.1) != "FALSE") {
            df.item.criteria.1 <- df.variables %>%
                dplyr::filter(questions == input$questions.factors.criteria.1) %>%
                dplyr::filter(class == "factor")
            
            selectInput("criteria",
                        label = "Criteria",
                        choices = l.variables[l.variables %in% df.item.criteria.1$name]
                        )
        } else {
            selectInput("criteria",
                        label = "Criteria",
                        choices = l.new.data,
                        selected = "One group"
                        )
        }
    })

    output$v.country  <- renderUI({
        if (input$comp.filtered.data == "no") {
            checkboxGroupInput("filter.category",
                               label = "Data not filtered",
                               choices = "all",
                               selected = "all"
                               )
        } else {
        checkboxGroupInput("filter.category",
                           label = "Select the category you want to include in plots",
                           choices = sort(unique(data$category)),
                           inline=TRUE
                           )
        }
    })


    ## Texts of the main questions and criteria
    ## ----------------------------------------

    output$mainquestion <- renderText({
        input$update
        paste0("Question ",input$question," :", df.questions[df.questions$questions==input$question,'main'])
    })

    output$criteria <- renderText({
        input$update
        paste0("Criteria (color) ",input$criteria," :", df.variables[df.variables$name==input$criteria,'item'])
    })


    ## Main plots
    ## ----------

    ## === ggPlotly ===
    ## output$survey.plotly <- renderPlotly({
    ##     ## output$survey.plot <- renderPlot({
    ##     input$update
    ##     ggpl <- isolate(ggplotly(do.call(input$graph.type,list(input$question,input$criteria))))
    ##     ggpl$elementId <- NULL
    ##     ggpl
    ## })

    ## === ggplot2 ===
    output$survey.plot <- renderPlot({
        ## output$survey.plot <- renderPlot({        
        input$update

        ggpl <- f.surv.plot(input$question,input$criteria,'all',input$op.bar.text,input$op.bar)
        graph <- (ggpl + theme_classic(base_size=14)
            + theme(legend.position="bottom", legend.direction = "horizontal")
            )
        return(graph)
        ## f.surv.num , f.surv.tf , f.surv.nef
    })
    
    output$survey.plot.filtered <- renderPlot({
        if (input$comp.filtered.data == 'no') {
            return(invisible())
        } else {
        ## output$survey.plot <- renderPlot({        
        input$update

        ggpl <- f.surv.plot(input$question,input$criteria,input$filter.category,input$op.bar.text,input$op.bar)
        graph <- (ggpl + theme_classic(base_size=14)
            + theme(legend.position="bottom", legend.direction = "horizontal")
            + ggtitle(paste("Filter :", paste(input$filter.category, collapse = ",")))
        )
        return(graph)
        ## f.surv.num , f.surv.tf , f.surv.nef
        }
    })

    ## Main tables
    ## -----------
    ## Info on the survey
    output$table.info.questions <- DT::renderDataTable({
        df.questions[c("questions","main")]
    })

    output$table.info.variables <- DT::renderDataTable({
        df.variables[c("questions","main","name","item")]    
    })
    
    ## A contingency table with the answers of the selected big questions
    output$survey.data.count <- renderTable({
        ## input$update        
        ## df.surv.text <- isolate(f.surv.text(input$question))
        df.surv.count.table <- f.surv.count.table(input$question,input$criteria, input$filter.category)
        
        df.surv.count.table
        
    })
    ## output$survey.data.count <- renderText({"blababla"})

    ## A contingency table with the answers of the selected big questions
    output$survey.data.median <- renderTable({
        ## input$update        
        ## df.surv.text <- isolate(f.surv.text(input$question))
        df.surv.median.table <- f.surv.median.table(input$question,input$criteria, input$filter.category)
        
        df.surv.median.table
        
    })
    ## output$survey.data.count <- renderText({"blababla"})

    
    ## A table with the answers of the selected big questions
    output$survey.data.set <- renderTable({
        ## input$update        
        df.surv.text <- f.surv.text(input$question) %>%
            dplyr::select(-id)
        
        df.surv.text 
    })
    
    ## Full table 
    ## ----------
    ## BUG not working !
    
    data.full.sel <- reactive({
        req(list(input$questions.full, input$criteria))
        data.full.sel <- f.surv.df.full.bq(input$questions.full,input$criteria)
        names(data.full.sel) <- variables[names(data.full.sel)]
        data.full.sel
    })

    ## A table with the answers of the selected big variables
    output$survey.data <- renderTable({
        ## input$update
        ## DT::datatable(
        data.full.sel()##,
        ## options = list(paging = FALSE)
        ## )
    })


    ## Text answers
    ## ------------

    ## === Free text question ===
    output$survey.text <- renderUI({
        input$update
        paragraph <- f.surv.paragraph(input$question,input$criteria, input$filter.category)
        return(paragraph)
    })
    
    ## === Other fields ===
    output$survey.other <- renderUI({
        input$update
        if ('op.other' %in% input$text.type) {
            if (!df.questions[input$question,c("other")]) {
                paragraph <- shiny::tags$h3("No other fields with free text for this question")
            } else {

                df.surv.other <- f.surv.other(input$question,input$criteria, input$filter.category)
                l.other <- list()
                
                l.other[[1]] <- shiny::tags$h2("Other fields with free text")
                l.other <- c(l.other,
                             lapply(df.surv.other,
                                    function(i) shiny::tags$li(i))
                             )
                paragraph <- l.other
            }
        } else if (df.questions[input$question,c("other")]) {
            paragraph <- shiny::tags$h2("Other fields with free text available for this question (option!!)")
        } else {
            paragraph <- shiny::tags$h4("No other fields with free text for this question")
        }
        paragraph
    })

    ## === Comments ===
    output$survey.comment <- renderUI({
        input$update
        if ('op.comment' %in% input$text.type) {
            df.surv.comment <- f.surv.comment(input$question,input$criteria, input$filter.category)

            if (dim(df.surv.comment)[1] < 1) {  
                paragraph <- shiny::tags$h3("No comment for this question")
            } else {        
                l.comment <- list()
                for (item in levels(as.factor(df.surv.comment$name))) {
                    l.comment <- c(l.comment,
                                   lapply(df.variables[df.variables$name == item,'item'],
                                          function(i) shiny::tags$h2(i))
                                   )
                    l.comment <- c(l.comment,
                                   lapply(df.surv.comment$comment[df.surv.comment$name == item],
                                          function(i) shiny::tags$li(i))
                                   )
                }   
                paragraph <- l.comment
            }
        } else if (df.questions[input$question,c("comment")]) {
            paragraph <- shiny::tags$h2("Comment(s) available for this question (option!!)")
        } else {
            paragraph <- shiny::tags$h4("No comment for this question")
        }
        paragraph
    })

    ## Maps
    ## ----
    output$survey.map <- renderPlot({
        input$update
        ggpl <- isolate(f.map.factor(input$criteria) ## %>%
                        ## layout(legend = list(orientation = 'h'
                        ##                     )
                        )  
        
        ## ggpl <- isolate(ggplotly(f.map.factor(input$criteria)) %>%
        ##                 layout(legend = list(orientation = 'h'
        ##                                      )
        ##                        )
        ##                 )
        ggpl$elementId <- NULL
        ggpl
    })

    ## Reporting
    ## ---------

    output$downloadReport <- downloadHandler(
        filename = function() {
            paste('my-report', sep = '.',
                  switch(
                      input$formatreport,
                      PDF = 'pdf',
                      HTML = 'html',
                      Word = 'docx'
                  ))
        },

        content = function(file) {
            owd <- setwd(tempdir())
            on.exit(setwd(owd))
            
            file.copy(src, 'temp_report_CDE.R', overwrite = TRUE)

            out <- f.reporting.question(input$quesion,
                                 input$criteria,
                                 input$formatreport
                                 )
        file.rename(out, file)
        }
    )


    ## Full Reporting
    ## --------------

    output$questions.report <- renderTable({
        df.questions[ , c("main","n.sub","class")]
    })
                                           

    output$downloadFullReport <- downloadHandler(
        filename = function() {
            paste('my-report', sep = '.',
                  switch(
                      input$formatfullreport,
                      PDF = 'pdf',
                      HTML = 'html',
                      Word = 'docx'
                  ))
        },

        content = function(file) {
            owd <- setwd(tempdir())
            on.exit(setwd(owd))
            
            file.copy(src, 'temp_report_CDE.R', overwrite = TRUE)

            out  <- f.reporting.survey(input$criteria,
                                       input$formatfullreport
                                       )
            
            file.rename(out, file)
            

        }
    )
}
